// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// The fraction-exponent representation and the hidden bit

flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
flpn = flps_numberformat ( flps , -0.3125 );
flps_numberprint(flps,flpn);

d = flps_tobary ( 1.25 , 2 , 3 )

flpn = flps_numberformat ( flps , 0.125 );
flps_numberprint(flps,flpn);


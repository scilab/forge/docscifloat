// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// For a number x in 0 <= x < b, 
// returns the b-ary digits d(i), i= 1, p such that 
// x = d(1) + d(2)/ b + d(3)/ b^2 + ... + d(p)/b^(p-1)
function d = barydigits ( b , p , x )
  d = []
  q = 1
  d(1) = ceil(x/b)
  x = x - d(1) * q
endfunction


// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Source : http://interstices.info/jcms/c_5936/le-dilemme-du-fabricant-de-tables-ou-comment-calculer-juste
// Sylvie Boldo
// Joanna Jongwane
// 14/04/2008
//
//
// Assume a base 10 machine, with precision p=6
// We are looking for the sin of t
t = 2.18998
sin(t)
// z is the exact answer
z = 0.81435250000019
// x and y are two possible values: should we choose x or y ?
x = 0.814352
y = 0.814353
// We compute |z-x| and |z-y|, approximately the same, but y is closer
abs(z-x)
abs(z-y)
// Conclusion: to say that y is the correctly rounded result to 6 digits,
// z has to be computed with 13 digits.
//=================================================================
// let t = 0.011111111001110110011101110011100111010000111101101101
// be stored in a IEEE double
// z = sin(t) = 0.01111010011001010100000111001100001100010001101001010111111111111111111111111111111111111111111111111111111111111111111100001011101001
// There are 66 ones after the 53-th bit
// The correctly rounded result is Z = 0.011110100110010101000001110011000011000100011010010110
// Conclusion: in order to compute sin(x) rounded toward -\infty,
// we need in this case 120 digits. If not, we cannot decide if the result 
// is lower or greater than Z.



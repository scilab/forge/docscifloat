// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Funny things about rounding :
// Because of rounding, mathematical equalities are not satisfied
// by floating point numbers.

format(25)
0.1 // is represented by 0.1000000000000000055511 = rounded over
0.7 // is represented by 0.6999999999999999555911 = rounded below
0.6 // is exactly representable
0.7-0.6 == 0.1 // is false !

// Another example
0.9 // is represented by 0.9000000000000000222045
1-0.9 == 0.1 // is false !

// Another example with multiplication
0.9 / 3 * 3 == 0.9 // is false !
// But 0.9 / 2 * 2 == 0.9 is true, because multiplication and division by 2 is exact, 
// provided that there is no overflow or underflow.

// Commutativity is satisfied by floating point numbers : x + y = y + x.
// But not associativity w/r addition i.e. (x+y)+z = x+(y+z)
// and associativity w/r multiplication i.e. x*(y*z) = (x*y) * z
// and distributivity i.e. x*(y+z) = x*y + x*z

// http://www.cs.princeton.edu/introcs/91float/
// http://www.macaulay.ac.uk/fearlus/floating-point/
(0.1 + 0.2) + 0.3 == 0.1 + (0.2 + 0.3) // False !
(0.1 * 0.2) * 0.3 == 0.1 * (0.2 * 0.3) // False !
0.3 * (0.1 + 0.2) == (0.3 * 0.1) + (0.3 * 0.2) // False !

// checkassociativityadd --
//   Returns %T if the associativity is true for the addition
function r = checkassociativityadd ( x , y , z )
  a = x + (y + z)
  b = (x + y) + z
  r = ( a == b )
endfunction
// Consider the numbers 0,0.1,0.2,...,0.9,1.0.
// There are 48 such numbers x<y<z so that the associativity does not hold for addition.
mprintf ("Searching for x, y, z so that :\n")
mprintf ("x + (y + z) != (x + y) + z\n")
total = 0
for x = linspace(0,1,11)
  for y  = linspace(0,1,11)
    for z = linspace(0,1,11)
      if ( x < y & y < z ) then
        r = checkassociativityadd ( x , y , z );
        if ( ~r ) then
          total = total + 1;
          mprintf ("Problem #%d: %.1f + (%.1f + %.1f) <> (%.1f + %.1f) + %.1f\n",total,x,y,z,x,y,z)
        end
      end
    end
  end
end

// checkdistributivitymult --
//   Returns %T if the distributivity is true for the multiplication
function r = checkdistributivitymult ( x , y , z )
  a = x * (y + z)
  b = x * y + x * z // Same as (x * y) + (x * z)
  r = ( a == b )
endfunction
// Consider the numbers 0,0.1,0.2,...,0.9,1.0.
// There are 47 such numbers x<y<z so that the distributivity does not hold for addition.
mprintf ("Searching for x, y, z so that :\n")
mprintf ("x * (y + z) != x*y + x*z\n")
total = 0
for x = linspace(0,1,11)
  for y  = linspace(0,1,11)
    for z = linspace(0,1,11)
      if ( x < y & y < z ) then
        r = checkdistributivitymult ( x , y , z );
        if ( ~r ) then
          total = total + 1;
          mprintf ("Problem #%d: %.1f * (%.1f + %.1f) <> %.1f*%.1f + %.1f*%.1f\n",total,x,y,z,x,y,x,z)
        end
      end
    end
  end
end

// checkassociativitymult --
//   Returns %T if the associativity is true for the multiplication
function r = checkassociativitymult ( x , y , z )
  a = x * (y * z)
  b = ( x * y ) * z
  r = ( a == b )
endfunction
// Consider the numbers 0,0.1,0.2,...,0.9,1.0.
// There are 29 such numbers x<y<z so that the associativity does not hold for multiplication
mprintf ("Searching for x, y, z so that :\n")
mprintf ("x * (y * z) != (x * y) * z\n")
total = 0
for x = linspace(0,1,11)
  for y  = linspace(0,1,11)
    for z = linspace(0,1,11)
      if ( x < y & y < z ) then
        r = checkassociativitymult ( x , y , z );
        if ( ~r ) then
          total = total + 1;
          mprintf ("Problem #%d: %.1f * (%.1f * %.1f) <> (%.1f * %.1f ) * %.1f\n",total,x,y,z,x,y,z)
        end
      end
    end
  end
end

// Playing with sines:
sin(%pi)

x=19*(1+%i);
cos(x)^2+sin(x)^2

// Playing with matrices
A = 1.e15 * rand(5,5);
exp(log(A)) - A

